//***MODELS - PRODUCT***
//Modules and Dependencies
	const mongoose = require('mongoose');




//Product Schema/Blueprint
	const productSchema = new mongoose.Schema({
		name: {
			type: String,
			required: [true, 'Name is Required']
		},
		description: {
			type: String,
			required: [true, 'Description is Required']
		},
		price: {
			type: Number,
			required: [true, 'Price is Required']
		},
		stockOnHand: {
			type: Number,
			required: [true, 'Number of stocks is Required']
		},
		isActive: {
			type: Boolean,
			default: true
		},
		createdOn: {
					type: Date,
					default: new Date()
		},
		category: {
			type: String,
			default: ''
		},
		tags: [{
			type: String,
			default: ''
		}],
		Reviews: [{
			name: {
				type: String,
				required: [true, 'Name is Required']
			},
			rating: {
				type: Number,
				default: 0
			},
			comment: {
				type: String,
				required: [true, 'Comment is Required']
			}
		}],
		Ratings: [{
			type: Number,
			default: 0,
			max: 5
		}],
		productOrders: [{
			orderId: {
				type: String,
				required: true
			}
		}]


	})


	


//Model
	module.exports = mongoose.model('Product', productSchema)
	




//Test Cart
	/*const CartSchema = new mongoose.Schema(
	  {
	    userId: {
	      type: mongoose.Schema.Types.ObjectId,
	      ref: "User"
	    },
	    products: [
	      {
	        productId: Number,
	        quantity: Number,
	        name: String,
	        price: Number
	      }
	    ],
	    active: {
	      type: Boolean,
	      default: true
	    },
	    modifiedOn: {
	      type: Date,
	      default: Date.now
	    }
	  },
	  { timestamps: true }
	);

	module.exports = mongoose.model("Cart", CartSchema);*/