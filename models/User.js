//***MODELS - USER***
//Modules and Dependencies
	const mongoose = require('mongoose');


//User Schema/Blueprint
	const userSchema = new mongoose.Schema({
		name: {
			type: String,
			required: [true, 'Name is Required'],
			unique: true
		},
		email: {
			type: String,
			required: [true, 'Email is Required'],
			unique: true
		},
		password: {
			type: String,
			required: [true, 'Password is Required']
		},
		isAdmin: {
			type: Boolean,
			default: false
		},
		shippingAddress: {
			type: String,
			required: [true, 'Address is Required']
		},
		paymentMethod: {
			type: String,
			required: [true, 'Payment Method is Required']
		},
		cardNumber: {
			type: Number,
			required: [true, 'Card Number is Required']
		},
		isNewUser: {
			type: Boolean,
			default: true
		},
		userOrders: [{
				productName: {
					type: String,
					required: [true, 'Product Name is Required']
				},
				productQuantity: {
					type: Number
					/*required: [true, 'Product Quantity is Required'],
					min: [1, 'Quantity should not be less than 1']*/
				},
			price: Number,
			totalAmount: {
				type: Number
			},
			purchasedOn: {
				type: Date,
				default: new Date()
			},
			status: {
				type: String,
				default: 'Ordered'
			}
		}],
		dateRegisteredOn: {
			type: Date,
			default: new Date()
		}
	})


//Model
	module.exports = mongoose.model('User', userSchema)