//***ROUTES - USER***
//Dependencies and Modules
	const exp = require('express');
	const controller = require('../controller/users');
	const auth = require('../auth');
	const User = require('../models/User');


//Routing Component
	const route = exp.Router();


//Verify
	const { verify, verifyAdmin } = auth;


//Routes - Get All User
	route.get('/all', (req, res) => {
		controller.getAllUser(req).then(result => res.send (result));
	});


//Routes - Get This User
	route.get('/thisUser/:userId', (req, res) => {
		controller.getThisUser(req.params.userId).then(result => res.send (result));
	});


//Routes - Register User
	route.post('/registerUser', (req, res) => {
		console.log(req.body);
		let userInfo = req.body;
		controller.register(userInfo).then(outcome => {
			res.send(outcome);
		});
	});


//Routes - User Authentication
	route.post('/login', (req, res) => {
		controller.loginUser(req.body).then(result => res.send(result))
	});


//Route - Promote To Admin
	route.put('/:userId/enable', verify, verifyAdmin, (req, res) => {
		controller.enableAdmin(req.params.productId).then(result => res.send(result));
	});


//Route - Demote To User
	route.put('/:userId/enable', verify, verifyAdmin, (req, res) => {
		controller.disableAdmin(req.params.productId).then(result => res.send(result));
	});

//Route - Create Order
	route.post('/checkout', auth.verify, controller.order);


//##
//Get all orders (user)
	route.get('/userOrders/:userId', verify, controller.getUserOrders);


//##
//Get all orders (admin)
	route.get("/allOrders", verify, verifyAdmin, (req,res) => {
	controller.getAllOrders().then(result => res.send(result))
})


//##
//Cancel This Order
	route.delete("/cancelOrder/:orderId", verify, (req,res) => {
	controller.cancelOrder(req.params.orderId).then(result => res.send(result))
})


//Expose Route System
	module.exports = route;


