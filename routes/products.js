//***ROUTES - PRODUCT***
//Dependencies and Modules
const exp = require('express');
const route = exp.Router();
const ProductController = require('../controller/products');
const auth = require('../auth');


//Routing Component
	// const route = exp.Router();


//Verify
	const { verify, verifyAdmin } = auth;


//#1
//Route - Create Product
	route.post('/createProduct', verify, verifyAdmin, (req, res) => {
		ProductController.addProduct(req.body).then(result => res.send(result));
	});


//#2
//Route - Get All Product
	route.get('/allProduct', verify, verifyAdmin, (req, res) => {
		ProductController.getAllProducts().then(result => res.send(result));
	});


//#3
//Route - Get All Active Product
	route.get('/activeProduct', (req, res) => {
		ProductController.getActiveProducts().then(result => res.send(result));
	});


//#4
//Route - Get This Product
	route.get('/:productId', (req, res) => {
		ProductController.getProduct(req.params.productId).then(result => res.send(result));
	});


//#5
//Route - Update Product Information
	route.put('/updateProduct/:productId', verify, verifyAdmin, (req, res) => {
		ProductController.updateProduct(req.params.productId, req.body).then(result => res.send(result));
	});


//#6
//Route - Archiving A Product
	route.put('/archive/:productId', verify, verifyAdmin, (req, res) => {
		ProductController.archiveProduct(req.params.productId).then(result => res.send(result));
	})


//#7
//Route - Re-enabling A Product
	route.put('/:productId/enable', verify, verifyAdmin, (req, res) => {
		ProductController.enableProduct(req.params.productId).then(result => res.send(result));
	})


//#8
//Route - Deleting A Product
	route.delete('/:productId/delete', verify, verifyAdmin, (req, res) => {
		ProductController.deleteProduct(req.params.productId).then(result => res.send(result));
	})


//#11
//Route - Get All Categories
	route.get('/allCategory', verify, verifyAdmin, (req, res) => {
		ProductController.getAllCategories().then(result => res.send(result));
	});







//#12
//Route - Get Specific Category



//#13
//Route - Create New Category



//#14
//Route - Update Category



//#15
//Route - Delete Category


//#21
//Route - Add A Review





//Expose Route System
	module.exports = route;