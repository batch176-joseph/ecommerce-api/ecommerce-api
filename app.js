//***APP.JS***
//[SECTION] Dependencies and Modules
	const express = require('express');
	const mongoose = require('mongoose');
	const dotenv = require('dotenv');
	const userRoutes = require('./routes/users');
	const productRoutes = require('./routes/products');
	const orderRoutes = require('./routes/orders');


//[SECTION] Environment Setup
	dotenv.config();
	const cred = process.env.CRED;
	const port = process.env.PORT;


//[SECTION] Server Setup
	const app = express();
	app.use(express.json());


//[SECTION] Database Connection
	mongoose.connect(cred, {
		useNewUrlParser: true,
		useUnifiedTopology: true
	});
	const status = mongoose.connection;
	status.once('open', () => console.log('Database collection Connected'));


//[SECTION] Backend Routes
	app.use('/users', userRoutes);
	app.use('/products', productRoutes);


//[SECTION] Server Gateway Response
	app.get('/', (req, res) => {
		res.send(`Ecommerce API`);
	})
	app.listen(port, () => {
		console.log(`Ecommerce API is Hosted on port ${port}`);
	})





