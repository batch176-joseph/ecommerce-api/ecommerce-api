//***CONTROLLER - PRODUCT***
//Dependencies and Modules
	const Product = require('../models/Product');
	// const Cart = require('../models/Cart');
	const bcrypt = require('bcrypt');
	const dotenv = require('dotenv');
	const auth = require('../auth');
	let newCart = [];

//Environment Setup
	dotenv.config();
	const salt = process.env.SALT;


//#1
//Controller - Create Product
	module.exports.addProduct = (product) => {
		let newProduct = new Product({
			name: product.name,
			description: product.description,
			price: product.price,
			stockOnHand: product.stockOnHand
		});
		return newProduct.save().then((product, error) => {
			if (error) {
				return false;
			} else {
				return product;
			}
		}).catch(error => error.message);
	};


//#2
//Controller - Get All Product
	module.exports.getAllProducts = () => {
		return Product.find({}).then(result => {
			return result;
		}).catch(error => error);
	};


//#3
//Controller - Get All Active Product
	module.exports.getActiveProducts = () => {
		return Product.find({isActive:true}).then(result => {
			return result;
		}).catch(error => error);
	};


//#4
//Controller - Get This Product
	module.exports.getProduct = (reqParams) => {
		return Product.findById(reqParams).then(result => {
			return result;
		}).catch(error => error);
	};


//#5
//Controller - Update Product Information
	module.exports.updateProduct = (productId, prod) => {
		let updatedProduct = {
			name: prod.name,
			description: prod.description,
			price: prod.price
		}
		return Product.findByIdAndUpdate(productId, updatedProduct).then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		}).catch(error => error);
	};


//#6
//Controller - Archiving A Product
	module.exports.archiveProduct = (productId) => {
		let updateActiveStatus = {
			isActive: false
		};
		return Product.findByIdAndUpdate(productId, updateActiveStatus).then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		}).catch(error => error);
	};


//#7
//Controller - Re-enabling A Product
	module.exports.enableProduct = (productId) => {
		let updateActiveStatus = {
			isActive: true
		};
		return Product.findByIdAndUpdate(productId, updateActiveStatus).then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		}).catch(error => error);
	};


//#8
//Controller - Deleting A Product
	module.exports.deleteProduct = (productId) => {
		return Product.findByIdAndDelete(productId).then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		}).catch(error => error);
	};


//#11
//Controller - Get All Categories
	module.exports.getAllCategories = () => {
		return Product.category.find({}).then(result => {
			return result;
		}).catch(error => error);
	};






//#12
//Controller - Get Specific Category



//#13
//Controller - Create New Category



//#14
//Controller - Update Category



//#15
//Controller - Delete Category


//#21
//Controller - Add A Review


