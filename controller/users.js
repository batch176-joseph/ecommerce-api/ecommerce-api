//***CONTROLLER - USER***
//Dependencies and Modules
	const User = require('../models/User');
	const bcrypt = require('bcrypt');
	const dotenv = require('dotenv');
	const auth = require('../auth');
	const Product = require('../models/Product');


//Environment Setup
	dotenv.config();
	const salt = process.env.SALT;


//Controller - Get All User
	module.exports.getAllUser = (data) => {
		return User.find().then(result => {
			result.password = '';
			return result;
		})
	}


//Controller - Get This User
	module.exports.getThisUser = (userId) => {
		return User.findById(userId).then(result => {
			result.password = '';
			return result;
		})
	}


//Controller - Register User
	module.exports.register = (userInfo) => {
		let name = userInfo.name
		let email = userInfo.email;
		let pass = userInfo.password;
		let shippingAddress = userInfo.shippingAddress;
		let paymentMethod = userInfo.paymentMethod;
		let cardNumber = userInfo.cardNumber;
		let newUser = new User({
			name: name,
			email: email,
			password: bcrypt.hashSync(pass, parseInt(salt)),
			shippingAddress: shippingAddress,
			paymentMethod: paymentMethod,
			cardNumber: cardNumber
		})
		return newUser.save().then((user, err) => {
			if (user) {
				return user;
			} else {
				return ({message: 'Unable to register user'});
			}

		}).catch(error => error.message);
	}


//Controller - User Authentication
	module.exports.loginUser = (data) => {
		return User.findOne({ email: data.email }).then(result => {
			if (result === null) {
				return false;
			} else {
				const isPasswordCorrect = bcrypt.compareSync(data.password, result.password)
				if (isPasswordCorrect) {
					return { accessToken: auth.createAccessToken(result.toObject()) }
				} else {
					//password do not match
					return false;
				}
			}
		});
	};


//Controller - Promote To Admin
	module.exports.enableAdmin = (userId) => {
		let updateAdminStatus = {
			isAdmin: true
		};
		return User.findByIdAndUpdate(userId, updateActiveStatus).then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		}).catch(error => error);
	};


//Controller - Demote To User
	module.exports.disableAdmin = (userId) => {
		let updateAdminStatus = {
			isAdmin: false
		};
		return User.findByIdAndUpdate(userId, updateActiveStatus).then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		}).catch(error => error);
	};


//Controller - Create Order
	module.exports.order = async (req,res) => {
		if(req.user.isAdmin) {
			return res.send({ message: "Action Forbidden"})
		}
		let isUserUpdated = await User.findById(req.user.id).then(user => {
			let newOrder = {
					productName: req.body.productName,
					productQuantity: req.body.productQuantity,
					price: req.body.price,
					totalAmount: req.body.price*req.body.productQuantity
			};
			user.userOrders.push(newOrder);
			return user.save().then(user => true).catch(err => err.message);

			if (isUserUpdated !== true) {
				return res.send({message: isUserUpdated})
			}
		})
	//find productId
		let isProductUpdated = await Product.findById(req.body.productId).then(product => {
			let prodOrder = {
				orderId: req.user.id
			}
			product.productOrders.push(prodOrder);
			return product.save().then(product => true).catch(err => err.message)
			if (isProductUpdated !== true) {
				return res.send({message: isProductUpdated})
			}
		})
		if (isUserUpdated && isProductUpdated) {
			return res.send({message: "Order successfully submitted."})
		}
	};


//##
//Get all orders (user)
	module.exports.getUserOrders = (req,res) => {
	User.findById(req.params.id, {email:0,password:0,isAdmin:0,shippingAddress:0,paymentMethod:0,cardNumber:0,isNewUser:0,dateRegisteredOn:0}).then(result => {
		return result
	}).catch(error => error);
}



//##
//Get all orders (admin)
	module.exports.getAllOrders = () => {
	return User.find({},{email:0,password:0,isAdmin:0,shippingAddress:0,paymentMethod:0,cardNumber:0,isNewUser:0,dateRegisteredOn:0}).then(result => {
		return result
	}).catch(error => error);
}


//##
//Cancel This Order
	module.exports.cancelOrder = (orderId) => {
		console.log(orderId,'1');
		if(req.user.isAdmin) {
			return res.send({ message: "Action Forbidden"})
		}
		console.log(orderId,'2');
		return user.userOrders.findByIdAndDelete(orderId).then((order, error) => {
			console.log(orderId,'3');
			if (error) {
				return false;
			} else {
				return true;
			}
		}).catch(error => error);
	};

